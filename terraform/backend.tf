terraform {
  backend "s3" {
    bucket = "jasif-jenkins-terraform"
    key = "jasif-jenkins-terraform.tfstate"
    region = "ca-central-1"
  }
}