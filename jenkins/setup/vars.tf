variable "AWS_REGION" {
  default = "ca-central-1"
}
variable "PATH_TO_PRIVATE_KEY" {
  default = "mykey"
}
variable "PATH_TO_PUBLIC_KEY" {
  default = "mykey.pub"
}
variable "AMIS" {
  type = map(string)
  default = {
    us-east-1 = "ami-0885b1f6bd170450c"
    us-west-2 = "ami-07dd19a7900a1f049"
    ca-central-1 = "ami-02e44367276fe7adc"
  }
}
variable "INSTANCE_DEVICE_NAME" {
  default = "/dev/xvdh"
}
variable "JENKINS_VERSION" {
  default = "2.263.3"
}
variable "TERRAFORM_VERSION" {
  default = "0.14.6"
}

variable "APP_INSTANCE_COUNT" {
  default = "0"
}
